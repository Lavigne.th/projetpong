﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetPong
{
    public class MyUselessForm
    {
        private int XCoord;
        private int YCoord;

        private int Width = 100;

        private static SolidBrush BlueBrush = new SolidBrush(Color.Blue);

        public MyUselessForm(int xCoord, int yCoord)
        {
            xCoord = XCoord;
            
            yCoord = YCoord;
        }

        public void Update()
        {
            XCoord += 1;
            YCoord += 1;
        }

        public void Render(Graphics g)
        {
            g.FillRectangle(BlueBrush, XCoord, YCoord, Width, Width);
        }

    }
}
