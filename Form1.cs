﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetPong
{
    public partial class Form1 : Form
    {
        private bool Running = false;

        private MyUselessForm MyForm = new MyUselessForm();

        private Player Player1 = new Player();

        public Form1() 
        {
            InitializeComponent();

            MainLoop();
        }

        private async void MainLoop()
        {
            await Task.Delay(2000);
            Running = true;
            while(Running)
            {
                await Task.Delay(100);
                Refresh();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;

            UpdateGame();
            RenderGame(g);
        }

        private void UpdateGame()
        {
            MyForm.Update();
            Player1.Update();
        }

        private void RenderGame(Graphics e)
        {
            MyForm.Render(e);
            Player1.Render(e);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
