﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ProjetPong
{
    public class Player
    {
        private int XCoord;
        private int YCoord;

        private int Width = 10;

        private static SolidBrush BlackBrush = new SolidBrush(Color.Black);

        public Player (int xCoord, int yCoord)
        {
            xCoord = XCoord;
            yCoord = YCoord;
        }
        public void Update()
        {


        }

        public void Render(Graphics g)
        {
            g.FillRectangle(BlackBrush, XCoord, YCoord, Width, Width);
        }
    }

}
